// TypeScript file

class StaticFile {
    public static API = {
        "ON_SET_OBSERVE_MODE" : 150,
        "TO_UPDATE_POSITION"  : 151,
        "TO_GAME_TIME_OVER"   : 110
    };

    public static labelSize: number  = 16;
    
    public static tankSize: number   = 25;
    public static bulletSize: number = 2;
    public static damageSize: number = 20;
}