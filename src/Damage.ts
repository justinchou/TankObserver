class Damage extends egret.Shape{
    public id: number;

    public w: number;
    public h: number;

    public color: number;

	public constructor() {
    	super();
	}

	public createBullet():void{
        var colorA, colorB, alpha, playerSize, lineThickness;

        colorA = 0xff0000;
        colorB = 0xff0000;
        playerSize = StaticFile.damageSize;
        lineThickness = 5;

        this.color = colorB;

        this.graphics.beginFill(colorA);
        this.graphics.drawCircle(this.w / 2, this.h / 2 , playerSize);
        this.graphics.endFill();

        this.anchorOffsetX = this.w / 2;
        this.anchorOffsetY = this.h / 2;
	}
}