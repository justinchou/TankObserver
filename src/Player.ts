class Player extends egret.Shape{
    public id: number;
    public isBot: boolean;

    public w: number;
    public h: number;

    public color: number;

	public constructor() {
    	super();
	}

	public createPlayer():void{
        var colorA, colorB, alpha, playerSize, lineThickness;

        if (this.isBot) {
            colorA = 0xffffff;
            colorB = 0xff0000;
            alpha = 0;
            playerSize = StaticFile.tankSize;
            lineThickness = 6;
        } else {
            colorA = Math.floor(Math.random() * 16777215);
            colorB = Math.floor(Math.random() * 16777215);
            alpha = 0;
            playerSize = StaticFile.tankSize;
            lineThickness = 8;
        }
        this.color = colorB;

        if (!this.isBot) {
            this.graphics.beginFill(colorA, alpha);
            this.graphics.lineStyle(lineThickness, colorB);
            this.graphics.drawRect(0, 0, this.w, this.h);
            this.graphics.endFill();
        }

        this.graphics.beginFill(colorA, alpha);
        this.graphics.lineStyle(lineThickness, colorB);
        this.graphics.drawCircle(this.w / 2, this.h / 2 , playerSize);
        this.graphics.endFill();

        this.anchorOffsetX = this.w / 2;
        this.anchorOffsetY = this.h / 2;
	}
}
