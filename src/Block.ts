// class Block extends egret.DisplayObjectContainer {
class Block extends egret.Shape {
    public id: number;
    public isBot: boolean;

    public w: number;
    public h: number;

    public color: number;

	public constructor() {
    	super();
	}

	public createBlock():void{
        var alpha, lineThickness;

        alpha = 1;
        lineThickness = 1;

        this.color = 0x4c4a48;

        // var sprite:egret.Sprite = new egret.Sprite();
        // sprite.graphics.beginFill(0xffffff, alpha);
        // sprite.graphics.lineStyle(lineThickness, this.color);
        // sprite.graphics.drawRect(0, 0, this.w, this.h);
        // sprite.graphics.endFill();
        // this.addChild(sprite);

        this.graphics.beginFill(0xffffff, alpha);
        this.graphics.lineStyle(lineThickness, this.color);
        this.graphics.drawRect(0, 0, this.w, this.h);
        this.graphics.endFill();

        this.anchorOffsetX = this.w / 2;
        this.anchorOffsetY = this.h / 2;
	}

    public createBlocks(blocks: Array<any>, scale): void {
        if (blocks.length <= 0) return;

        let blockData = blocks[0];
        this.scaleX     = scale;
        this.scaleY     = scale;
        
        this.x          = 0;
        this.y          = 0;

        let alpha = 1;
        let lineThickness = 1;
        this.color = 0x4c4a48;

        for (let i = 0; i < blocks.length; i++) {
            let blockData = blocks[i];

            this.graphics.beginFill(0xffffff, alpha);
            this.graphics.lineStyle(lineThickness, this.color);
            this.graphics.drawRect(blockData["position"]["x"], blockData["position"]["y"], blockData["box"]["x"] * scale / 2, blockData["box"]["y"] * scale / 2);
            this.graphics.endFill();
        }
    }
}
